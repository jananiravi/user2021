+++
title = "Partners & Instutional Support"
description = "Partners of the useR! 2021 conference."
keywords = ["community","groups", "R-Ladies", "MiR", "Forwards"]
+++


The useR! Organizing Committee is community based. We work on a voluntary basis, but also rely on the support of our institutions to put in all the hours that are needed to organize a global virtual conference. On this page, we name some of these institutions and R Communities and Organizations that provide invaluable input into the conference. 

## KOF Swiss Economic Institute, ETH Zurich (Host / Global Digital Partner)

KOF provides expert information in the field of economic and business cycle research. It produces a multitude of forecasts and indicators which are used to monitor the economy. Its data pool of company surveys is unique throughout Switzerland. KOF uses this data to generate various indicators (such as the KOF Economic Barometer, the KOF Employment Indicator and the KOF Business Situation Indicator), which reflect Swiss economic sentiment. It evaluates its survey findings on a quarterly basis and presents them – as well as its economic forecasts – to the public. Furthermore, it analyses the innovation activities of Swiss companies, publishes studies on the labour market and healthcare spending, and comments on the latest economic developments. KOF – together with the Munich-​based ifo Institute – is also involved in the Joint Economic Forecast for Germany, which the German government uses as a benchmark for the performance of the economic development.


## R-Ladies

Mission: R-Ladies is a worldwide organization whose mission is to promote Gender Diversity in the R community.
As a diversity initiative, the mission of R-Ladies is to achieve proportionate representation by encouraging, inspiring, and empowering people of genders currently underrepresented in the R community. R-Ladies’ primary focus, therefore, is on supporting minority gender R enthusiasts to achieve their programming potential, by building a collaborative global network of R leaders, mentors, learners, and developers to facilitate individual and collective progress worldwide.

### Contact
You can find more information on R-Ladies in our website https://rladies.org/ and follow us on Twitter: https://twitter.com/RLadiesGlobal.  if you are an R-Lady and want to start a chapter near you, please get in touch via info@rladies.org.


## Forwards

Forwards is a task force set up by the R foundation to improve diversity, equity and inclusion (DEI) in R Foundation related activities (R core, CRAN, useR!, etc). The task force addresses the underrepresentation of women as well as other groups such as LGBTQ, minority ethnic groups, and people with disabilities in the R community.

### Contact
You can find more information on Forwards here and follow us on Twitter: https://twitter.com/R_Forwards

## MiR

Short description
Our mission is to provide a space of belonging and support for people who identify as underrepresented minority R useRs. We also provide opportunities for underrepresented minority R useRs to contribute to the R community in various aspects. To learn more about why MiR was started, read this Medium post.

### Contact
Follow us on Twitter: https://twitter.com/miR_community
Complete this form to join MiR as a member or an ally: MiR Community Registration Form

<br><br><br>