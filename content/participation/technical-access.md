+++
title = "Accessibility Standards for useR! 2021 Technical notes"
description = "Guidelines and standards to make technical notes accessible."
keywords = ["Technical Notes","Accessibility"]
+++

# UseR! 2021 Accessibility Standards for Elevator pitches

Elevator pitches can be lightning talks or technical notes. For lightning talks, the general (guidelines for talks)[user2021.r-project.org/participation/talks-access/] apply. For technical notes, we kindly ask that prospective presenters: 

+ Prefer text-based platforms such as markdown or Beamer.
+ Add alt-text to images and plots that explains completely the features of the image. 


## Other reminders for Q&A sessions

+ Speak as clearly as possible, looking at the camera, and try not to go too fast
+ During the session, comment briefly on what it is that you are showing and why. Avoid explanations that rely only on obvious features that may not be obvious for everyone, such as "As you can see" or "The image speaks for itself"

For more information, check the useR!2021 [Accessibility Standards](user2021.r-project.org/participation/accessibility/) or contact [user2021-accessibility[at]r-project.org](mailto:user2021-accessibility@r-project.org)

<br>
<br>
<br>

