+++
title = "Keynote Speakers"
description = "Keynotes speakers for useR! 2021"
keywords = ["Responsible Programming","Communication","Research Software Enginneering","RSE"]
+++

<!-- 
From Research Software Engineering and Responsible Programming to Teaching and the R Core Keynote, useR! keynote cover a broad 

## Communication: 
Communicating results in accurate and efficient ways is key to making a real impact. A climate scientist, a journalist and a data journalist and designer will share their insights into communication with us. 


-->




<div class="row">
    <div class="col-md-4">
        <img src="/img/artwork/paul.png" alt="Picture of Keynote Speaker Paul Murrell">
    </div>
    <div class="col-md-8 col-lg-8">
        <div class="kn">R Core</div>
        <div class="kn-main">Paul Murrell</div>
        <div class="kn-sub">Associate Professor, University of Auckland, Aotearoa New Zealand</div>
        <p>
Paul is a member of R-core, mostly contributing to the graphics system.  He has also developed several R add-on packages with a recent focus on integrating R graphics with other graphics systems.
</p>
    </div>
</div>


<br><br>


<div class="row">
    <div class="col-md-4">
        <img src="/img/artwork/edzer.png" alt="Picture of Keynote Speaker Edzer Pebesma">
    </div>
    <div class="col-md-8 col-lg-8">
        <div class="kn">Statistics</div>
        <div class="kn-main">Edzer Pebesma</div>
        <div class="kn-sub">Professor, Universität Münster, Germany</div>
        <p>Edzer has been a Full professor in geoinformatics since 2007, an R user and R spatial developer since 2002, one of the editors-in-chief for the Journal of Statistical Software, and an ordinary member of the R foundation.</p>
    </div>
</div>



<br><br>


<div class="row">
    <div class="col-md-4">
        <img src="/img/artwork/heidi.png" alt="Picture of Keynote Speaker Heidi Seibold">
    </div>
    <div class="col-md-8 col-lg-8">
        <div class="kn">RESEARCH SOFTWARE ENGINEERING</div>
        <div class="kn-main">Heidi Seibold</div>
        <div class="kn-sub">Group Leader, Helmholtz Zentrum Munich, Germany</div>
        <p>
Heidi is a principal investigator at Helmholtz AI, where she leads a group on Open AI in Health. Her research lies at the intersection of open science, data science and health research. She has been involved in the development of R packages including partykit, model4you, and OpenML. She co-founded the Zurich R User Group and co-organized useR! 2020.
</p>
    </div>
</div>


<br><br>


<div class="row">
    <div class="col-md-4">
        <img src="/img/artwork/jeroen.png" alt="Picture of Keynote Speaker Jeroen Ooms">
    </div>
    <div class="col-md-8 col-lg-8">
        <div class="kn">R Programing</div>
        <div class="kn-main">Jeroen Ooms</div>
        <div class="kn-sub">UC Berkeley, USA</div>
        <p>
 Jeroen is a researcher and software developer with the rOpenSci group. He has written (too) many CRAN packages, and also maintains the compilers and build infrastructure for R on Windows.
</p>
    </div>
</div>


<br><br>


<div class="row">
    <div class="col-md-4">
        <img src="/img/artwork/meenakshi.png" alt="Picture of Keynote Speaker Meenakshi Kushwaha">
    </div>
    <div class="col-md-8 col-lg-8">
        <div class="kn">R in Action</div>
        <div class="kn-main">Meenakshi Kushwaha</div>
        <div class="kn-sub">Co-Founder and Director of Research, ILK Labs, Bangalore</div>
        <p>
Meenakshi is an air quality and environmental health researcher and recently moved to Taiwan. She works on projects based in India and in the US. Meenakshi is a co-founder of women-owned business ILK Labs (a research and education consulting organization). She is passionate about reproducible research and enjoys teaching R and stats. Her work on pollution inequities was recently featured in the <a href="https://www.nytimes.com/interactive/2020/12/17/world/asia/india-pollution-inequality.html">New York Times</a>

</p>
    </div>
</div>


<br><br>


<div class="row">
    <div class="col-md-4">
        <img src="/img/artwork/alberto.png" alt="Picture of Keynote Speaker Alberto Cairo"><br><br>
        <img src="/img/artwork/catherine.png" alt="Picture of Keynote Speaker Catherine Gicheru"><br><br>
        <img src="/img/artwork/katharine.png" alt="Picture of Keynote Speaker Katharine Hayhoe">
    </div>
    <div class="col-md-8 col-lg-8">
        <div class="kn">Communication</div>
        <div class="kn-main">JOINT KEYNOTE</div>
        <div class="kn-sub">Alberto Cairo, Catherine Gicheru, Katharine Hayhoe </div>
        <p>
Communicating results in accurate and efficient ways is key to making a real impact. A climate scientist, a journalist and a data journalist and designer will share their insights into communication with us.
</p>
<p>
<b class="speaker">Alberto</b> is a professor of visualization at the University of Miami and author of several books, such as 'How Charts Lie' (2019). He has been a graphics director at news publications in Spain and Brazil. Today, he is also a consultant and freelancer with companies, educational institutions, and government agencies.
</p>
<p>
<b class="speaker">Catherine</b> is an International Center for Journalists (ICFJ) Knight Fellow currently spearheading the ​Africa Women Journalism Project ​which is driving the coverage of underreported gender, health and development issues affecting marginalized groups and strengthening the voices of women journalists by helping them to become innovators in their newsrooms. A veteran journalist with two of the leading media organisations in the region, Catherine co-founded East Africa’s first budget and public finance fact checking and verification initiative, ​​PesaCheck​. She is a Nieman Fellow, a Reuters Institute for the Study of Journalism Fellow and an IWMF Courage in Journalism award winner.
</p>
<p>
<b class="speaker">Katharine</b> is an atmospheric scientist whose research focuses on understanding what climate change means for people and the places where we live. She is an endowed professor of public policy and law in the Dept. of Political Science at Texas Tech University, she hosts the PBS digital series Global Weirding, and she has been named one of TIME's 100 Most Influential People, the United Nations Champion of the Environment, and the World Evangelical Alliance’s Climate Ambassador. 
</p>

</div>
</div>


<br><br>



<div class="row">
    <div class="col-md-4">
        <img src="/img/artwork/achim.png" alt="Picture of Keynote Speaker Achim Zeileis"><br><br>
        <img src="/img/artwork/dorothy.png" alt="Picture of Keynote Speaker Dorothy Gordon">
    </div>
    <div class="col-md-8 col-lg-8">
        <div class="kn">Responsible Programming</div>
        <div class="kn-main">JOINT KEYNOTE</div>
        <div class="kn-sub">Jonathan Godfrey, Kristian Lum, Achim Zeileis, Dorothy Gordon</div>
        <b class="speaker">Jonathan</b> is a Lecturer of Statistics at Massey University, Aotearoa New Zealand. His research is focused on the needs of the thousands of blind people around the world who need additional tools to make the visual elements of statistical thinking and practice less of a barrier. He is the official disability advisor of Forwards. 
</p>
<p>
<b class="speaker">Kristian</b> a statistician who spent several years working at a human rights non-profit. She now works in a computer science department. Her research focuses on algorithmic (un)fairness, especially as it relates to the use of algorithms in the criminal justice system. 
</p>
<p>
<b class="speaker">Achim</b> is a Professor of Statistics at the Faculty of Economics and Statistics at Universität Innsbruck. Being an R user since version 0.64.0, Achim is co-author of a variety of CRAN packages such as zoo, colorspace,
party(kit), sandwich, or exams. In the R community he is active as an ordinary member of the R Foundation, co-creator of the useR! conference series, and co-editor-in-chief of the open-access Journal of Statistical Software.
</p>
<p>
<b class="speaker">​​Dorothy</b> is a global leader in the field of technology and development with a special focus on Africa. She works to bring about greater engagement and action on policy, implementation and evaluation issues relating to the impact of technology on society. She is a Pan-Africanist and a feminist.​​ Her commitment to Open Source technologies stems from the fact that they build greater technology ownership, innovation and diversity.


</div>
</div>


<br><br>

<div class="row">
    <div class="col-md-4">
        <img src="/img/artwork/metadocencia.png" alt="meta docencia logo">
    </div>
    <div class="col-md-8 col-lg-8">
        <div class="kn">Teaching</div>
        <div class="kn-main">Metadocencia</div>
        <div class="kn-sub">community of Spanish-speaking educators</div>
        <p>
Metadocencia is a non-profit organization that nurtures a community of Spanish-speaking educators by teaching concrete, evidence-based, and student-centered educational methods. They collaboratively develop open, reusable, and accessible resources to foster effective training practices.

</p>


</div>
</div>


<br><br><br>