+++
title = "Sponsoring useR! 2021"
description = "Want to support the R Stats Language for Statistical Computing and its world class community? Learn how to help and benefit from the reach of the useR! conference."
keywords = ["conference","sponsor","support","open source"]
+++

# Learn How to Help and Benefit From the Reach of useR!

<a href="/img/partners-n-sponsors_sponsors_useR_SPONSORING_V2.2.pdf" target="_blank">**Download Sponsoring Brochure**</a>

**The useR! conference** brings together users and developers of the R Language for Statistical Computing from around the world on a yearly basis. In 2021, the conference will be entirely virtual for the first time making the creation of a truly global, community-based, inclusive and innovative experience its number one priority. The organization committee has reimagined what can be, and is aiming to set a new bar for what should be. You as a sponsor, -- you should also be a part of this R-evolution.

**By sponsoring useR! 2021 you support the global R community and contribute to its ongoing development. It is an outstanding opportunity to reach a large group of highly talented data science professionals.**

Are you interested? - Please contact us via **useR2021-sponsoring [ at ] r-project.org**! 


# Your Spot at useR!
 
In various combinations and packages, we offer the following benefits to our sponsors:
 
- Free conference or tutorial attendees
 
- Logo on website, mentioning the sponsorship level
- Virtual Booth for interaction with participants
- Sponsor a Session or a Tutorial (Logo on slides between talks, mention at the beginning and end of Session/Tutorial)
- Day Sponsor: Banner/slide on screen during breaks, mentioned before keynotes
 
- Prime Time Sponsored Lightning Talk (<5min, just before the keynote)
- Sponsored Talk in Session (marked as sponsored)
- Recruitment or Company profile video (available in virtual booth and on the conference website)
 
- Social Media Posts (Twitter, LinkedIn)


Fully virtual events are new to most of us. We leverage the virtual nature of the event to make it more accessible and more global. We want to make sure that you get the best out of your sponsoring engagement of this virtual event and are happy to discuss your suggestions. In every case, you have the opportunity to demonstrate commitment to R users around the world that have been ready to bring their knowhow forward to the global stage and have had limited opportunities to do so and to stand with a conference committed to ensuring accessibility in as many ways as possible.

# Sponsorship Packages
{{<table "table table-striped table-bordered">}}
|                                                | Platinum | Gold | Silver | Bronze | Networker |
|------------------------------------------------|----------|------|--------|--------|-----------|
| Free conference attendees                      |        5 |    3 |      2 |      2 |         2 |
| Free slots to attend tutorials                 |        1 |    1 |      1 |        |           |
| Logo in website footer                         |        1 |      |        |        |           |
| Listed with Logo on website                    |        1 |    1 |      1 |      1 |           |
| Company profile on website                     |        1 |    1 |        |        |           |
| Virtual Booth                                  |        1 |    1 |      1 |      1 |         1 |
| Sponsored Sessions or Tutorials                |        3 |    2 |      1 |        |           |
| Main sponsor of the day                        |        1 |      |        |        |           |
|                                                |          |      |        |        |           |
| **Sponsored Content**                          |          |      |        |        |           |
| Prime Time Sponsor Lightning Talk (<5min)      |        1 |      |        |        |           |
| Sponsor Talk in Session  (marked as sponsored) |        1 |    1 |      1 |        |           |
| Recruitment or Company profile video           |        1 |    1 |      1 |      1 |           |
|                                                |          |      |        |        |           |
| **Communication**                              |          |      |        |        |           |
| Social Media Post                              |        5 |    3 |      2 |      1 |         1 |
{{</table>}}
<br>
Numbers represent counts, e.g., number of free attendees, number of sponsored sessions. The maximum number of Sponsors is five for each sponsorship level (Platinum, Gold, etc.). The number of Networker Sponsors is not limited.


Pricing details, including a thorough explanation how we adapt prices for companies from different countries can be found here: [Sponsorship pricing and Cost Conversion](https://user2021.r-project.org/sponsoring/package/), our you can consult 
<a href="/img/partners-n-sponsors_sponsors_useR_SPONSORING_V2.2.pdf" target="_blank">our brochure</a>
<br>
<br>
<br>
<br>
